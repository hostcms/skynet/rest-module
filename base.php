<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');

//set_error_handler(function($err_no, $err_str, $err_file, $err_line ) {
//	throw new ErrorException($err_str, 0, $err_no, $err_file, $err_line);
//});
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 01.10.2015
 * Time: 19:31
 */
abstract class Skynetrest_Base extends Core_Servant_Properties
{
	// 1xx: Informational (Информационные).
	const CODE_CONTINUE = 100;							// => 'Continue', // (Продолжать).
	const CODE_SWITCHING_PROTOCOLS = 101;				// => 'Switching Protocols', // (Переключение протоколов).
	const CODE_PROCESSING = 102;						// => 'Processing', // (Идет обработка).

	//2xx: Success (Успешно).
	const CODE_OK = 200;								// => 'OK', // (Хорошо).
	const CODE_CREATED = 201;							// => 'Created', // (Создано).
	const CODE_ACCEPTED = 202;							// => 'Accepted', // (Принято).
	const CODE_NON_AUTHORITATIVE = 203;					// => 'Non-Authoritative Information', // (Информация не авторитетна).
	const CODE_NO_CONTENT = 204;						// => 'No Content', // (Нет содержимого).
	const CODE_RESET_CONTENT = 205;						// => 'Reset Content', // (Сбросить содержимое).
	const CODE_PARTIAL_CONTENT = 206;					// => 'Partial Content', // (Частичное содержимое).
	const CODE_MULTI_STATUS = 207;						// => 'Multi-Status', // (Многостатусный).
	const CODE_IM_USED = 226;							// => 'IM Used', // (IM использовано).

	//3xx: Redirection (Перенаправление).
	const CODE_MULTIPLE_CHOICES = 300;					// => 'Multiple Choices', // (Множество выборов).
	const CODE_MOVED_PERMANENTLY = 301;					// => 'Moved Permanently', // (Перемещено окончательно).
	const CODE_FOUND = 302;								// => 'Found', // (Найдено).
	const CODE_SEE_OTHER = 303;							// => 'See Other', // (Смотреть другое).
	const CODE_NOT_MODIFIED = 304;						// => 'Not Modified', // (Не изменялось).
	const CODE_USE_PROXY = 305;							// => 'Use Proxy', // (Использовать прокси).
	const CODE_RESERVED = 306;							// => '', // (зарезервировано).
	const CODE_TEMPORARY_REDIRECT = 307;				// => 'Temporary Redirect', // (Временное перенаправление).

	//4xx: Client Error (Ошибка клиента).
	const CODE_BAD_REQUEST = 400;						// => 'Bad Request', // (Плохой запрос).
	const CODE_UNAUTHORIZED = 401;						// => 'Unauthorized', // (Неавторизован).
	const CODE_PAYMENT_REQUIRED = 402;					// => 'Payment Required', // (Необходима оплата).
	const CODE_FORBIDDEN = 403;							// => 'Forbidden', // (Запрещено).
	const CODE_NOT_FOUND = 404;							// => 'Not Found', // (Не найдено).
	const CODE_METHOD_NOT_ALLOWED = 405;				// => 'Method Not Allowed', // (Метод не поддерживается).
	const CODE_NOT_ACCEPTABLE = 406;					// => 'Not Acceptable', // (Не приемлемо).
	const CODE_PROXY_AUTHENTICATION_REQUIRED = 407;		// => 'Proxy Authentication Required', // (Необходима аутентификация прокси).
	const CODE_REQUEST_TIMEOUT = 408;					// => 'Request Timeout', // (Время ожидания истекло).
	const CODE_CONFLICT = 409;							// => 'Conflict', // (Конфликт).
	const CODE_GONE = 410;								// => 'Gone', // (Удален).
	const CODE_LENGTH_REQUIRED = 411;					// => 'Length Required', // (Необходима длина).
	const CODE_PRECONDITION_FAILED = 412;				// => 'Precondition Failed', // (Условие «ложно»).
	const CODE_REQUEST_ENTITY_TOO_LARGE = 413;			// => 'Request Entity Too Large', // (Размер запроса слишком велик).
	const CODE_REQUEST_URI_TOO_LONG = 414;				// => 'Request-URI Too Long', // (Запрашиваемый URI слишком длинный).
	const CODE_UNSUPPORTED_MEDIA_TYPE = 415;			// => 'Unsupported Media Type', // (Неподдерживаемый тип данных).
	const CODE_REQUESTED_RANGE_NOT_SATISFIABLE = 416;	// => 'Requested Range Not Satisfiable', // (Запрашиваемый диапазон не достижим).
	const CODE_EXPECTATION_FAILED = 417;				// => 'Expectation Failed', // (Ожидаемое не приемлемо).
	const CODE_IM_TEAPOT = 418;							// => 'I\'m a teapot', // (Я - чайник).
	const CODE_UNPROCESSABLE_ENTITY = 422;				// => 'Unprocessable Entity', // (Необрабатываемый экземпляр).
	const CODE_LOCKED = 423;							// => 'Locked', // (Заблокировано).
	const CODE_FAILED_DEPENDENCY = 424;					// => 'Failed Dependency', // (Невыполненная зависимость).
	const CODE_UNORDERED_COLLECTION = 425;				// => 'Unordered Collection', // (Неупорядоченный набор).
	const CODE_UPGRADE_REQUIRED = 426;					// => 'Upgrade Required', // (Необходимо обновление).
	const CODE_RETRY_WITH = 449;						// => 'Retry With', // (Повторить с...).
	const CODE_UNRECOVERABLE_ERROR = 456;				// => 'Unrecoverable Error', // (Некорректируемая ошибка...).

	//5xx: Server Error (Ошибка сервера).
	const CODE_INTERNAL_SERVER_ERROR = 500;				// => 'Internal Server Error', // (Внутренняя ошибка сервера).
	const CODE_NOT_IMPLEMENTED = 501;					// => 'Not Implemented', // (Не реализовано).
	const CODE_BAD_GATEWAY = 502;						// => 'Bad Gateway', // (Плохой шлюз).
	const CODE_SERVICE_UNAVAILABLE = 503;				// => 'Service Unavailable', // (Сервис недоступен).
	const CODE_GATEWAY_TIMEOUT = 504;					// => 'Gateway Timeout', // (Шлюз не отвечает).
	const CODE_HTTP_VERSION_NOT_SUPPORTED = 505;		// => 'HTTP Version Not Supported', // (Версия HTTP не поддерживается).
	const CODE_VARIANT_ALSO_NEGOTIATES = 506;			// => 'Variant Also Negotiates', // (Вариант тоже согласован).
	const CODE_INSUFFICIENT_STORAGE = 507;				// => 'Insufficient Storage', // (Переполнение хранилища).
	const CODE_BANDWIDTH_LIMIT_EXCEEDED = 509;			// => 'Bandwidth Limit Exceeded', // (Исчерпана пропускная ширина канала).
	const CODE_NOT_EXTENDED = 510;						// => 'Not Extended' // (Не расширено).

	protected $_options = array();
	/**
	 * Valid object properties
	 * @var array
	 */
	protected $_validProperties = array();

	protected $_instance = array();

	protected $_retValue = array();

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions($options)
	{
		$this->_options = $options;
		return $this;
	}

	/**
	 * Connect constructor.
	 */
	public function __construct($options=array())
	{
		parent::__construct();

		$this->_instance = \Core_Page::instance();
		$this->_retValue = [
			'message' => 'Неизвестная операция',
			'code' => self::CODE_METHOD_NOT_ALLOWED,
		];
		foreach($_GET as $getRequestKey => $getRequestValue) {
			$propertyName = 'get'.ucfirst(strtolower($getRequestKey));
			$this->addAllowedProperty($propertyName); //  _allowedProperties['get_'.$getRequestKey] = $getRequestValue;
			$this->$propertyName = $getRequestValue;
		}
		$postWorking = array();
		switch (\Core_Array::get($_SERVER, "CONTENT_TYPE", 'default')) {
			case 'application/json':
				$inputJSON = file_get_contents('php://input');
				$postWorking = json_decode($inputJSON, true);
				break;
			default:
				$postWorking = $_POST;
		}
		if(is_array($postWorking)) {
			foreach($postWorking as $postRequestKey => $postRequestValue) {
				$propertyName = 'post'.ucfirst(strtolower($postRequestKey));
				$this->addAllowedProperty($propertyName);
				$this->$propertyName = $postRequestValue;
			}
		}
		if(is_array($_FILES) && count($_FILES)>0) {
			foreach($_FILES as $filesRequestKey => $filesRequestValue) {
				$propertyName = 'file'.ucfirst(strtolower($filesRequestKey));
				$this->addAllowedProperty($propertyName);
				$this->$propertyName = $filesRequestValue;
			}
		}
		$this->_options = $options;
	}

	public function getValue($property, $defaults)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			return $this->_propertiesValues[$property];
		}

		return $defaults;
	}

	public function getValues()
	{
		return $this->_propertiesValues;
	}

	/**
	 * Utilized for reading data from inaccessible properties
	 * @param string $property property name
	 * @return mixed
	 */
	public function __get($property)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			return $this->_propertiesValues[$property];
		}

		throw new \Core_Exception("The property '%property' does not exist in '%class'.",
			array('%property' => $property, '%class' => get_class($this)));
	}

	/**
	 * @param array $validProperties
	 */
	public function addValidProperty($validProperty)
	{
		$this->_validProperties[] = $validProperty;
	}

	/**
	 * @return array
	 */
	public function getValidProperties()
	{
		return $this->_validProperties;
	}

	/**
	 * @param array $validProperties
	 */
	public function setValidProperties($validProperties)
	{
		$this->_validProperties = $validProperties;
	}

	/**
	 * Run when writing data to inaccessible properties
	 * @param string $property property name
	 * @param string $value property value
	 * @return self
	 */
	public function __set($property, $value)
	{
		if (array_key_exists($property, $this->_propertiesValues))
		{
			$this->_propertiesValues[$property] = $value;
			return $this;
		}

		throw new \Core_Exception("The property '%property' does not exist in the entity",
			array('%property' => $property));
	}

	/**
	 * Check cache availability
	 * @return boolean
	 */
	public static function createInstance() {}

	public static function encodeUrl($str) {
		return str_replace('/', '\/', $str);
	}

	/**
	 */
	private function _validInputValues() {
		if(count($this->_validProperties)>0) {
			if(is_array($this->_validProperties) && count($this->_validProperties)>0) {
//				print_r($this->_propertiesValues);
//				print_r($this->_validProperties);
//				foreach ($this->_validProperties as $validPropertyKey => $validProperty) {
//					$this->_validateExpression($validPropertyKey, $validProperty);
//				}
				return true;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	private function _validateExpression($vpKey, $vpValues) {
		$retValue = false;

		foreach ($vpValues as $vpValueKey => $vpValue ) {
			if(is_array($vpValue)) {
				switch ($vpKey) {
					case 'AND':
						$retValue = $retValue & (isset($this->_propertiesValues[$vpValue]));
						break;
					case 'OR':
						$retValue = $retValue | (isset($this->_propertiesValues[$vpValue]));
						break;
				}
			} else {

			}
		}
//		if(isset($this->_propertiesValues[$vpValue])) {
//			$retValue = true;
//		}
		return $retValue;
	}

	/**
	 */
	public function execute() {
		return $this->_validInputValues();
	}
}