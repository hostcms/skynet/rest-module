<?php
use Skynetrest_Connect as rest;
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

//set_error_handler(function($err_no, $err_str, $err_file, $err_line ) {
//	throw new ErrorException($err_str, 0, $err_no, $err_file, $err_line);
//});
/**
 * Core command controller.
 *
 * @package HostCMS
 * @subpackage Core\Command
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2018 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Skynetrest_Controller extends Core_Command_Controller
{
	protected $_header = '';

	/**
	 * @return mixed
	 */
	public function getHeader()
	{
		return $this->_header;
	}

	/**
	 * @param mixed $header
	 */
	public function setHeader($header)
	{
		$this->_header = $header;
	}

	public function showAction()
	{
		restore_exception_handler();
		set_error_handler(function ($severity, $message, $file, $line) {
			throw new \ErrorException($message, $severity, $severity, $file, $line);
		});
		try {
			$prevErrorReporting = '';
			$route = Skynetrest_Core_Router::getRoute("{$this->needmodule}_rest_api");

			ob_start();
			if($route !== false) {
				if( Core_Array::get($_SERVER, 'HTTP_ACCEPT', 'none') != 'application/json'  ) {
					$_SERVER['HTTP_ACCEPT'] = 'application/json';
				}
				if( preg_match('/application\/x-www-form-urlencoded/', ($sct = Core_Array::get($_SERVER, 'CONTENT_TYPE', 'none')))  ) {
					$this->_header = 'application/x-www-form-urlencoded';
				}
				$partPrefix = str_replace('{needmodule}', $this->needmodule, $route['pattern']);
				if(isset($this->api_version)) {
					$partPrefix = str_replace('{api_version}', $this->api_version, $partPrefix);
				}
				$partSplitted = array_values(array_filter(array_map(function ($item) {
					if($item != '') {
						return Core_Str::ucfirst($item);
					}
				}, explode('/', $partPrefix))));
				list($module) = $partSplitted;

				$prevErrorReporting = error_reporting();

				$basicLogin = $basicPassword = '';
				$domain = Core_Page::instance()->skynet->request->domain;
				if(isset($moduleConfig[CURRENT_SITE]['rest'])
					&& isset($moduleConfig[CURRENT_SITE]['rest'][$domain])
					&& isset($moduleConfig[CURRENT_SITE]['rest'][$domain]['user'])
					&& isset($moduleConfig[CURRENT_SITE]['rest'][$domain]['password'])
				) {
					$basicLogin = $moduleConfig[CURRENT_SITE]['rest'][$domain]['user'];
					$basicPassword = $moduleConfig[CURRENT_SITE]['rest'][$domain]['password'];
				}
				$rest = rest::createInstance($partPrefix, '', [
						'stop_class' => [
							'list'
						]
					])
					->setAuthFormMessage('REST-API. athenticate, please')
					->setRestBasicLoginAndPassword($basicLogin, $basicPassword)
					->addNamespace('\\'.implode('_',$partSplitted))
					->execute()
				;
			}
			$response = ob_get_clean();
		} catch (Exception $e) { // \Skynetcore_Exception_Core $e) {
			$response = json_encode([
				"err_message" => [
					"code" => $e->getCode(),
					"file" => $e->getFile(),
					"line" => $e->getLine(),
					"previous" => $e->getPrevious(),
					"message" => $e->getMessage(),
				],
				"message" => "ERROR",
				"code" => Skynetrest_Base::CODE_INTERNAL_SERVER_ERROR,
			]);
		}
		restore_error_handler();

		if($response=='') {
			$response = '{"message": "Ошибка аутентификации", "code": 401}';
		}
		error_reporting($prevErrorReporting);
		$oCore_Response = new Core_Response();
		switch ($this->_header) {
			case 'application/x-www-form-urlencoded':
				$oCore_Response->header('Content-Type', $this->_header);
				$oCore_Response
					->body($response);
				break;
			default:
				$decodedResponse = json_decode($response);
				$oCore_Response
					->body($response);
		}
		$oCore_Response
			->status(isset($decodedResponse->code) ? $decodedResponse->code : 200)
		;

		return $oCore_Response;
	}
}