<?php
use Respect\Rest\Router as restRouter;
use Skynetcore_Utils as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 25.09.2015
 * Time: 9:47
 */
class Skynetrest_Connect {

	protected $_partPrefix = NULL;
	protected $_authFormMessage = 'Please, take login and password';
	protected $_restBasicLogin = NULL;
	protected $_restBasicPassword = NULL;
	protected $_userClass = '';
	protected $_options = '';

	protected $_namespaces = array('Skynetrest_Auth');

	/**
	 * Connect constructor.
	 */
	public function __construct($partPrefix='/', $userClass='', $options=[])
	{
		$this->_partPrefix = $partPrefix;
		$this->_userClass = $userClass;
		$this->_options = $options;
	}

	public static function createInstance($partPrefix='/rest', $userClass='', $options=[]) {
		return new self($partPrefix, $userClass, $options);
	}

	/**
	 * @return array
	 */
	public function getNamespaces()
	{
		return $this->_namespaces;
	}

	/**
	 * @param array $namespaces
	 */
	public function setNamespaces($namespaces)
	{
		$this->_namespaces = $namespaces;
		return $this;
	}

	/**
	 * @param array $namespaces
	 */
	public function addNamespace($namespace)
	{
		$this->_namespaces[] = $namespace;
		return $this;
	}

	/**
	 * @param array $namespaces
	 */
	public function setOption($optionKey, $option)
	{
		$this->_options[$optionKey] = $option;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAuthFormMessage()
	{
		return $this->_authFormMessage;
	}

	/**
	 * @param string $authFormMessage
	 */
	public function setAuthFormMessage($authFormMessage)
	{
		$this->_authFormMessage = $authFormMessage;
		return $this;
	}

	/**
	 * @param null $restBasicLogin
	 * @param null $restBasicPassword
	 */
	public function setRestBasicLoginAndPassword($restBasicLogin, $restBasicPassword)
	{
		$this->_restBasicLogin = $restBasicLogin;
		$this->_restBasicPassword = $restBasicPassword;
		return $this;
	}

	public function execute()
	{
		try {
			$r3 = new restRouter();
			$this->_options = array(array_merge($this->_options,
				array('namespaces'=>$this->_namespaces)
			));
//			$r3->errorRoute(function (array $err) {
//				$err->logError();
////				return 'Sorry, this errors happened: '.var_dump($err);
//			});
			$any = $r3
				->any($this->_partPrefix.'/**', 'Skynetrest_Service', $this->_options);
			if(trim($this->_restBasicLogin)!='' && trim($this->_restBasicPassword)!='') {
				$any
					->authBasic($this->_authFormMessage, function ($user, $pass) {
						return $user === $this->_restBasicLogin && $pass === $this->_restBasicPassword;
					});
			}
			$any
				->accept(array(
//					'application/xml' => function($input) {
//						return $input['entity'];
//					},
//					'text/html' => function($data) {
//						list($k,$v)=each($data);
//						return "$k: $v";
//					},
						'application/json' => function($input) {
							header('Content-type: application/json; charset=utf-8');
							if(isset($input['code']) && $input['code']!='') {
								header("HTTP/1.1 {$input['code']}");
							}
							return json_encode($input, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
						},
					)
				);

//		$post = $r3
//			->post($this->_partPrefix.'/**', 'Skynetrest_Service', $this->_options);
//		if(trim($this->_restBasicLogin)!='' && trim($this->_restBasicPassword)!='') {
//			$post
//				->authBasic($this->_authFormMessage, function ($user, $pass) {
//					return $user === $this->_restBasicLogin && $pass === $this->_restBasicPassword;
//				});
//		}
//		$post
//			->contentType(array(
//				'multipart/form-data' => function($input) {
//					parse_str($input, $output);
//					return $output;
//				},
//				'application/json' => function($input) {
//					return my_json_converter($input);
//				}
//			))
//			->accept(array(
//					'application/json' => function($input) {
//						header('Content-type: application/json; charset=utf-8');
//						if(isset($input['code']) && $input['code']!='') {
//							header("HTTP/1.1 {$input['code']}");
//						}
//						return json_encode($input, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
//					},
//				)
//			)
//		;
		} catch (Skynetcore_Exception_Core $e) {
			$e->logError();
		}
	}
}