<?php
use Skynetrest_Base;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 01.10.2015
 * Time: 19:31
 */
class Skynetrest_Auth_User extends Skynetrest_Base
{
    protected $_siteUser = NULL;

    /**
     * valid object properties
     * @var array
     */
    protected $_validProperties = array(
        'AND' => array(
            'OR' => array(
                array('getToken',),
                array('AND' => array('getLogin','getPassword',))
            )
        )
    );

    public static function createInstance($options=array()) {
        return new self($options);
    }

    public function execute() {
        if(parent::execute()) {
            $returns = $this->_checkAuth();
            /** @var \Siteuser_Model $oSiteUser */
            $oSiteUser = $returns['siteUser'];
            $tokenObject = $returns['token'];
            $retValue = array();
            if(!is_null($returns['siteUser']) && !is_null($returns['token'])) {
                $this->_siteUser = $oSiteUser;
                $suPeople = $oSiteUser->siteuser_people->findAll();
                $suName = isset($oSiteUser->name) ? $oSiteUser->name : '';
                $suSurname = isset($oSiteUser->surname) ? $oSiteUser->surname : '';
                $suPatronymic = isset($oSiteUser->patronymic) ? $oSiteUser->patronymic : '';

                if(isset($suPeople[0]) && isset($suPeople[0]->id) && $suPeople[0]->id>0) {
                    $suName = $suPeople[0]->name;
                    $suSurname = $suPeople[0]->surname;
                    $suPatronymic = $suPeople[0]->patronymic;
                }
                $results = array(
                    'profile' => array(
                        'basic' => array(
                            'id' => $oSiteUser->id,
                            'name' => $suName,
                            'surname' => $suSurname,
                            'middleName' =>  $suPatronymic,
                            'token' => $tokenObject->token,
                            'expires_at' => $tokenObject->expired_date,
                        ),
                    )
                );
                $retValue = array('result' => $results);
            }
            $retValue = array_merge(array(
                'message' => $returns['message'],
                'code' => $returns['code'],
            ), $retValue);
        } else {
            $retValue = array(
                'message' => "Неверно заданы входные параметры",
                'code' => Base::INVALID_PARAMS,
            );
        }

        return $retValue;
    }

    /**
     * @param $token
     * @return array
     */
    protected function _checkAuth(){
        /*
         * если токен задан
         *      если найден в базе, то проверка на время жизни
         *          если токен валиден, то го далее             202 валидный токен
         *          если время жизни вышло, то ошибка           403 устарел
         *      если не найден в базе, то ошибка                403 не валидный токен
         *если токен не задан
         *      если юзер найден в базе
         *          если валидный токен                         202 валидный юзер, вот токен
         *          если невалидный токен, пересоздать          202 пересоздан, вот токен
         *          если нет токена                             202 валидный юзер, вот токен
         *      если невалидный юзер                            400 невалидный юзер
         *
         */
        $login = $this->getValue('getLogin', $this->getValue('postLogin', 'fakeingLoginNotFound'));
        $password = $this->getValue('getPassword', $this->getValue('postPassword', 'fakeingPasswordNotFound'));
        $token = $this->getValue('getToken', $this->getValue('postToken', 'tokenNotFound'));

        $oSiteUser = NULL;
        $tokenObject = NULL;
        if(!is_null($tokenObject = $this->_getTokenInfoByToken($token))) {
            if(strtotime($tokenObject->expired_date) < strtotime(date('Y-m-d H:i:s'))){
                $checkResult['message'] = Base::REGISTRATION_LOGIN_EXISTS_CODE.' Forbidden. Токен устарел, обновите.';
                $checkResult['code'] = Base::REGISTRATION_LOGIN_EXISTS_CODE;
            } else {
                $oSiteUser = \Core_Entity::factory('Siteuser')->getById($tokenObject->siteuser_id);
                $checkResult['message'] = Base::PUSH_NOTIFICATION_SERVICE_INVALID_CODE.' Accepted. Пользователь найден, существующий токен валиден. {'.$tokenObject->token.'}';
                $checkResult['code'] = Base::PUSH_NOTIFICATION_SERVICE_INVALID_CODE;
            }
        } else {
            if($login!='fakeingLoginNotFound' && $password!='fakeingPasswordNotFound') {
                if(!is_null($oSiteUser = \Core_Entity::factory('Siteuser')->getByLoginAndPassword($login, $password)) && $oSiteUser->active==1) {
                    if(is_null($tokenObject = $this->_getTokenInfoBySiteuser($oSiteUser->id))) {
                        $tokenObject = $this->_createToken($oSiteUser);
                    }
                    $checkResult['message'] = Base::PUSH_NOTIFICATION_SERVICE_INVALID_CODE." Accepted. Пользователь найден, существующий токен валиден. {".$tokenObject->token."}";
                    $checkResult['code'] = Base::PUSH_NOTIFICATION_SERVICE_INVALID_CODE;
                } else {
                    $checkResult['message'] = Base::VALIDATION_ERROR_CODE." Bad Request. Пользователь с таким логином/паролем не найден.";
                    $checkResult['code'] = Base::VALIDATION_ERROR_CODE;
                }
            } else {
                $checkResult['message'] = Base::REGISTRATION_LOGIN_EXISTS_CODE.' Forbidden. Невалидный токен.';
                $checkResult['code'] = Base::REGISTRATION_LOGIN_EXISTS_CODE;
            }
        }
        $checkResult['token'] = $tokenObject;
        $checkResult['siteUser'] = $oSiteUser;

        return $checkResult;
    }

    /**
     * @param $request
     * @return null
     */
    protected function _getTokenInfoByToken($token){
        $token = \Core_Entity::factory('Rest_Token')->getByToken($token);
        if($token!=NULL && strtotime($token->expired_date) < strtotime(date('Y-m-d H:i:s'))){
            $token->delete();
        }
        return $token;
    }

    /**
     * @param $request
     * @return null
     */
    protected function _getTokenInfoBySiteuser($siteuser_id){
        $token = \Core_Entity::factory('Rest_Token')->getBySiteuser_Id($siteuser_id);
        if($token!=NULL && strtotime($token->expired_date) < strtotime(date('Y-m-d H:i:s'))){
            $token->delete();
        }
        return $token;
    }

    /**
     * @param $request
     * @return null
     */
    protected function _createToken($oSiteUser){
        $createDate = date('Y-m-d H:i:s');
        $expiredDate = date('Y-m-d H:i:s', strtotime('+1 day'));
        $token = \Core_Entity::factory('Rest_Token');
        $token->token = \Core_Hash::instance()->hash($oSiteUser->login . $oSiteUser->password . date('Y-m-d H:i:s'));
        $token->create_date = $createDate;
        $token->expired_date = $expiredDate;
        $token->siteuser_id = $oSiteUser->id;
        $token->save();
        return $token;
    }

    /**
     * @param $request
     * @return null
     */
    protected function _getTokenBySiteuserID($siteuser_id){
        $token = \Core_Entity::factory('Rest_Token')->getBySiteuser_Id($siteuser_id);
        return $token;
    }

}