<?php
use PtcHandyMan as ptc;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skynetrest_Core_Router extends Core_Router
{
	static public function getRouters()
	{
		return self::$_routes;
	}

	static public function getRoute($route)
	{
		if(isset(self::$_routes[$route])) {
			/** @var Core_Router_Route $route */
			$route = self::$_routes[$route];
			return [
				'pattern' => strstr(ptc::getProperty($route, '_uriPattern'), '{uri}', true),
				'path' => ptc::getProperty($route, '_controllerColumns')['uri'],
				'uri' => ptc::getProperty($route, '_uri'),
				'controller' => ptc::getProperty($route, '_controller'),
			];
		} else {
			return false;
		}
	}
}