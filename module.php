<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Skynet Rest Module.
 *
 * @package HostCMS 6\Module
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © mikeborisov, m.u.borisov@gmail.com
 */
class Skynetrest_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.1';

	/**
	 * Module date
	 * @var date
	 */
	public $date = '2019-07-01';

	/**
	 * Skynetcore_Module constructor.
	 * @param string $version
	 */
	public function __construct()
	{
		parent::__construct();
	}
}