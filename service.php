<?php
use Respect\Rest\Routable;
use Respect\Rest as rest;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 25.09.2015
 * Time: 9:47
 */
class Skynetrest_Service implements Routable
{
	/**
	 * @var array
	 */
	protected $_returns = array(
		'message' => Skynetrest_Base::CODE_OK." Ok.",
		'code' => Skynetrest_Base::CODE_OK,
	);

	protected $_options = array(
		'stop_class' => array()
	);

	/**
	 * Service constructor.
	 * @param array $_returns
	 */
	public function __construct($options=array())
	{
		$this->_options = $options;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param $operation
	 * @return array
	 */
	public function get($operation, $method = '')
	{
//		Skynetcore_Utils::v($operation, $method);
		$method==='' && $method=__FUNCTION__;
//		Skynetcore_Utils::p($operation, $method);
		$method=str_replace('.', '', ucfirst($method));
//		Skynetcore_Utils::p($operation, $method);
		if(isset($operation[0])) {
			$sOperation = str_replace('_', '', ucfirst(strtolower(preg_replace('~.json~', '', $operation[0]))));
			$sOperationList = [];
			if(($operationsCnt=count($operation))>1) {
				for ($operationKey=1; $operationKey<=$operationsCnt; $operationKey++) {
					if(isset($operation[$operationKey])) {
						$sTmp = mb_strtolower($operation[$operationKey]);
						$sOperation .= '_'.\Core_Str::ucfirst($sTmp);
						unset($operation[$operationKey]);
					}
					if(isset($this->_options['stop_class']) && in_array($sTmp, $this->_options['stop_class'])) {
						break;
					}
				}
				unset($operation[0]);
				$sOperationList = $operation;
			}
			$this->_options['stop'] = array_values($operation);
			$baseClass = $this->_options['namespaces'][0];
			$processingClass = str_replace('.', '', "{$baseClass}\\{$sOperation}");
			if(class_exists($processingClass)) {
				$this->_returns = $processingClass::createInstance()
					->execute();
			} else {
				$exception = true;
				$contentType = \Core_Array::get($_SERVER, 'HTTP_ACCEPT', 'text/html');
				switch ($contentType) {
					case 'application/xml':
						$_GET['responsetype'] = 'XMLCONSOLE';
						break;
					case 'application/json':
						$_GET['responsetype'] = 'ARRAY';
						break;
					default:
						$_GET['responsetype'] = 'HTML';
				}
				if(($optionsCount=count($this->_options['namespaces']))>1) {
					for($i=1; $i<$optionsCount; $i++) {
						$processingClass = str_replace('.', '', "{$this->_options['namespaces'][$i]}_{$method}_{$sOperation}");
						if(class_exists($processingClass)) {
							$processInstance = $processingClass::createInstance();
							$this->_returns = $processInstance
								->setOptions($this->_options)
								->execute();
							$exception=false;
							break;
						}
					}
				}

				$exception && $this->_returns = array(
					'message' => sprintf("Класс `%s` не существует.", $processingClass),
					'code' => Skynetrest_Base::CODE_UNPROCESSABLE_ENTITY,
				);
			}
		} else {
			$this->_returns = array(
				'message' => Skynetrest_Base::CODE_METHOD_NOT_ALLOWED." Bad Request. Неизвестная операция.",
				'code' => Skynetrest_Base::CODE_METHOD_NOT_ALLOWED,
			);
		}
		return $this->_returns;
	}

	/**
	 * @param $operation
	 * @return array
	 */
	public function post($operation)
	{
		return $this->get($operation, 'post');
	}

	/**
	 * @param $operation
	 * @return array
	 */
	public function put($operation)
	{
		return $this->get($operation, 'put');
	}
}